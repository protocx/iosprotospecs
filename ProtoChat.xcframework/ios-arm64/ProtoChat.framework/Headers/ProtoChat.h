//
//  ProtoChat.h
//  ProtoChat
//
//  Created by Le Dinh Tuan on 15/09/2021.
//  Copyright © 2021 Proto. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ProtoChat.
FOUNDATION_EXPORT double ProtoChatVersionNumber;

//! Project version string for ProtoChat.
FOUNDATION_EXPORT const unsigned char ProtoChatVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProtoChat/PublicHeader.h>


